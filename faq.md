---
layout: page 
title: Frequently Asked Questions
description: FAQ concerning the event and the venue
image: socrates_crete_2022_4.jpeg
---

> The list will be populated with more questions that might arise in the upcoming days.

<a class="anchor" id="dates"></a>
<p><font size="5em"><b><a href="#dates">The website states that the SoCraTes Crete event runs from the 29th of June to 2nd of July. Is it all 4 days?</a></b></font></p>
SoCraTes Crete runs from the evening of June 29th to early afternoon of July 2nd. Accommodation packages feature check-in on the 29th and check-out on the 2nd.
<hr />

<a class="anchor" id="co-attend"></a>
<p><font size="5em"><b><a href="#co-attend">I am planning to attend with a fellow SoCraTes Crete participant. How do we book our stay together at OAC (Orthodox Academy of Crete)?</a></b></font></p>
Both participants need to register via the website and select one of the two participant options that feature sharing a double room.
<hr />

<a class="anchor" id="accompanying"></a>
<p><font size="5em"><b><a href="#accompanying">I am planning to attend with an accompanying person. How do we book our stay at OAC?</a></b></font></p>
The participant needs to register as such by selecting one of the 2 options that feature the conference option and sharing a double room. The options are `Conference with accommodation at OAC sharing a double room, with lunch` and `Conference with accommodation at OAC sharing a double room, without lunch`.

The accompanying person must also register via the same page by selecting one of the two designated options. These options are `Accompanying person, sharing a double room at OAC, with lunch` and `Accompanying person, sharing a double room at OAC, without lunch`.

If you are not planning to stay within the OAC you have to select the `Conference facilities + coffee breaks (with lunch, no accommodation)` or `Conference facilities + coffee breaks (no lunch, no accommodation)` for the participant only.
<hr />

<a class="anchor" id="family"></a>
<p><font size="5em"><b><a href="#family">I am planning to attend with my family. How do we book our stay at OAC?</a></b></font></p>
If your partner is not a participant then they must register as an accompanying person as explained above. Conversely if they are a participant then they must register as one.

If your are traveling with children please note that children under 6 years of age are not required to register (and they don’t have to pay!). But please do let us know by email so that we can arrange one of the bigger rooms for you and your family.

Children between ages 6 and 12 will have to register but pay a discounted price of 50%. Individuals older than 12 pay the full price.
<hr />

<a class="anchor" id="payment"></a>
<p><font size="5em"><b><a href="#payment">I have registered via the website. How do I pay?</a></b></font></p>
OAC this year, due to staff shortage, have kindly requested for all participants to complete their payments via Bank transfer, one week in advance of the (un)conference.

Please make sure to only add the following text as comments for the transaction:

> [your name] SOCRATESCRETE

The IBAN and Bank Account info:

<table>
<tr><td>Account holder</td><td>Orthodox Academy of Crete</td></tr>
<tr><td>Bank name:</td><td>Cooperative Bank of Chania (Kolympari branch)</td></tr>
<tr><td>ACCOUNT No:</td><td>113505004</td></tr>
<tr><td>SWIFT CODE:</td><td>STXAGRA1</td></tr>
<tr><td>IBAN CODE:</td><td>GR20 0690 0080 0000 0011 3505 004  </td></tr>
</table>

Note: All bank charges are at the <b>sender’s expense</b>.
<hr />

<a class="anchor" id="refund"></a>
<p><font size="5em"><b><a href="#refund">I decided I will not attend SoCraTes Crete. Can I get a refund?</a></b></font></p>
Regrettably, <b>no</b>. The OAC – who is responsible for running the event and accepting payments, etc. – cannot offer refunds.
<hr />

<a class="anchor" id="invoice"></a>
<p><font size="5em"><b><a href="#invoice">I completed the payment via bank transfer to OAC (the venue) bank account. Will I receive an invoice? When?</a></b></font></p>
Yes! After your payment is received and processed by the OAC they will issue a special kind of invoice. In Greek this is called “Γραμμάτιο Είσπραξης” and in English: “Notice of Receipt”. This is so because OAC is a non-profit institution.

This invoice will clearly state your name and the fact you are participating to SoCraTes Crete. If this would not suffice for your company’s accounting requirements, OAC can additionally include your company’s name and VAT identification number. Please send these to our email as well in advance so that OAC can include them in your Notice of Receipt.

Please note that past experience -OAC has been organizing conferences for years- has shown that this should be sufficient for your needs.

You can collect this invoice during your stay at the OAC.

Below you will find a sample of what this Notice of Receipt looks like:

<img src="assets/images/sample-oac-notice-of-receipt-651x1024.png" alt="sample OAC receipt" />
<hr />

<a class="anchor" id="check-in-out"></a>
<p><font size="5em"><b><a href="#check-in-out">When should I check-in / check-out?</a></b></font></p>
Early check-in on the 29th of June might be possible. If you would like to check in even before the 29th please send us an email.

Unfortunately check-out this year is not as flexible and all participants must check-out by 9:00AM local time at latest on the 2nd of July.
<hr />

<a class="anchor" id="her-flight"></a>
<p><font size="5em"><b><a href="#her-flight">I am flying to HER airport. How do I get to OAC in Kolympari?</a></b></font></p>
There is no official airport transfer planned, so participants have to make their own way from the airport to the venue.

Traditionally, though, our participants are great at self-organization, so car-sharing (whether rental or taxi) has been pretty typical in previous years. We expect something similar will also happen this year. For this, we recommend joining the [SoCraTes Crete Discord](https://discord.gg/4Z45eHvZDJ), where you can find a #carsharing channel. Expect a shared spreadsheet closer to the event dates to help coordinate this.
<hr />

<a class="anchor" id="cha-flight"></a>
<p><font size="5em"><b><a href="#cha-flight">I am flying to CHA airport. How do I get to OAC in Kolympari?</a></b></font></p>
See above.
<hr />

<a class="anchor" id="covid-19"></a>
<p><font size="5em"><b><a href="#covid-19">Are there any special CoViD19 measures in place for the (un)conference?</a></b></font></p>
Please check our <a href="{{ '/covid-19.html' | relative_url }}" target="_blank">CoVid19 Policy page</a>.
<hr />

<a class="anchor" id="oac-kids"></a>
<p><font size="5em"><b><a href="#oac-kids">I am traveling with kids. Is the accommodation at OAC suitable for us?</a></b></font></p>
Our goal for SoCraTes Crete is to be a family-friendly event. This means that rooms that feature 3 beds or are spacier will be first reserved for families with young children. Please let us know if you are planning to bring your family along so we can arrange one of these rooms for you.

The rooms are a little “spartan”, so if you are planning for an “all-inclusive” type of holiday it might be best to book accommodation elsewhere. Having said that, the venue’s location is fantastic. There is a small beach right across the road, there is ample space on the venue without cars, etc. and most of our participants have found it most convenient when staying together with their families at the OAC.
<hr />

<a class="anchor" id="oac-cot"></a>
<p><font size="5em"><b><a href="#oac-cot">I am traveling with (little) kids. Is a cot offered by OAC or should we bring our own?</a></b></font></p>
A cot is not offered by OAC so you must bring your own, should you require one.
<hr />

<a class="anchor" id="kitchen"></a>
<p><font size="5em"><b><a href="#kitchen">Are kitchen amenities offered at OAC?</a></b></font></p>
There is a kitchen located in Building A, but kitchenware like kettles or pots are not offered so you should bring your own, should you require any.
<hr />

<a class="anchor" id="share-3-people"></a>
<p><font size="5em"><b><a href="#share-3-people">Is it possible for 3 people to share a room?</a></b></font></p>
While there are some rooms with 2 beds and a sofa bed or 3 beds, they are very very few and they will be reserved for families with young children. SoCraTes Crete is a family-friendly event, and with Crete being such a popular tourist destination many participants bring their families along. Please let us know if you are planning to bring your family along in advance so we can arrange one of these rooms for you.
<hr />

<a class="anchor" id="register-accompanying-people"></a>
<p><font size="5em"><b><a href="#register-accompanying-people">How can a register for accompanying people to share a room with me?</a></b></font></p>
At the sign up form you can select more than one options for the persons that will accompany you. If for instance someone want to attend the event but also want to bring his/her family (an under aged < 6y.o., an under aged > 6 y.o. and an accompanying adult) the registration form should look like this:
<img src="assets/images/socrates-sign-up-form.png" alt="sample sign up form" />
<hr />

<a class="anchor" id="register-sharing-room-participants"></a>
<p><font size="5em"><b><a href="#register-sharing-room-participants">How can a register for two people that both want to attend the event and share the room?</a></b></font></p>
The sign up process has to be submitted twice, each by each one participant. If you want to share a double room please contact the org team through <a href="mailto:{{ site.email }}">email</a> or <a href="{{ site.discord_url }}">our discord server</a> (See `@OrgTeam` members).
<hr />

<a class="anchor" id="what-packages-include"></a>
<p><font size="5em"><b><a href="#what-packages-include">What's included in the packages apart the accommodation and/or the conference?</a></b></font></p>
All accommodation packages include breakfast and lunch at the OAC. This means that packages 2-5 will offer breakfast and lunch.
<br>All Conference packages include coffee breaks. This means that packages 1-3 include coffee breaks during a break between sessions.
<hr />

<a class="anchor" id="update-registration"></a>
<p><font size="5em"><b><a href="#update-registration">I have changed my mind about the registration form I completed. Can I update it?</a></b></font></p>
You can update the details in the registration form you submitted by submitting the form again using the same email as the last time. Each previous submission will be ignored!
<hr />

<a class="anchor" id="after-registration"></a>
<p><font size="5em"><b><a href="#after-registration">I have submitted the registration form. What should I do next?</a></b></font></p>
* You can join our [Discord server](https://discord.gg/4Z45eHvZDJ) to keep up to date with all the SoCraTes Crete news and meet more participants.
* You can open a (Merge Request (MR))[https://gitlab.com/socrates-crete/site/-/tree/main#participants] to our repo with your name, profile picture and contact info to be added to our website at the participants section!
* You will receive an email with details about the payment process in the upcoming days.
<hr />


<style type="text/css">
    a.anchor {
        position: relative;
        top: -75px;
    }
    img {
        max-width: 100%;
    }
</style>