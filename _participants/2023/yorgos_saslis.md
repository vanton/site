---
full_name: Yorgos Saslis (Red Hat)
image: yorgos.jpg
website: https://gsaslis.github.io
linkedin: https://www.linkedin.com/in/gsaslis
mastodon: https://chaos.social/@yorgos
github: https://github.com/gsaslis
gitlab: https://gitlab.com/gsaslis 
---
