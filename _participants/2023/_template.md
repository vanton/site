---
full_name: Your_Name
image: link_to_your_profile_picture
website: link_to_your_website_profile
linkedin: link_to_your_linked-in_profile
twitter: link_to_your_twitter_profile
mastodon: link_to_your_mastodon-in_profile
facebook: link_to_your_facebook_profile
github: link_to_your_github_profile
gitlab: link_to_your_gitlab_profile
---
